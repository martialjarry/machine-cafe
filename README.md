# MACHINE À CAFÉ

Machine à café est un bot Node.js pour Discord. Il suffit de poster le message `!40centimes` suivi d'un nom de boisson dans un salon, et le bot vous préparera un café.

## Installation

Il est nécessaire de télécharger et d'installer Node.js sur votre ordinateur/serveur. [Cette page du tutoriel Node.js d'OpenClassrooms](https://openclassrooms.com/courses/des-applications-ultra-rapides-avec-node-js/installer-node-js) explique la procédure d'installation sur Windows, Mac OS X et Linux, et les bases du fonctionnement du terminal.

Téléchargez ensuite le fichier `machine_a_cafe.js`, et rangez-le dans un dossier spécifique sur votre ordinateur (il sera nécessaire de redémarrer le bots si le dossier ou ses fichiers sont déplacés, choisissez un emplacement sur votre ordinateur qui ne sera pas amené à changer si possible).

Depuis le terminal, placez vous ensuite dans le dossier spécifique, en tapant `cd CHEMIN_DU_DOSSIER` (exemple sur Windows : `cd \Users\JeanMichel\Desktop\mon_dossier_sur_le_bureau`) puis en appuyant sur Entrée. Laissez ensuite la fenêtre du terminal ouverte.

### Création du profil du bot sur Discord.

Sur votre navigateur favori, rendez-vous sur [la page de création de bot Discord](https://discordapp.com/developers/applications), cliquez sur "Nouvelle application". Nommez le bot "Machine à café" (ou tout autre nom que vous souhaitez lui donner, ce sera celui qui apparaîtra sur vos serveurs). Ajoutez une description et un avatar si vous le souhaitez, puis cliquez sur "Créer une app".

Repérez la section Bot du menu, et depuis ce menu, cliquez sur "Créez un utilisateur Bot". Confirmez, puis cliquez sur "Cliquer pour révéler le jeton" à côté du champ "Jeton". Copiez la suite de chiffres et de lettres qui apparaît alors.

### Lancement du bot.

Ouvrez le fichier `machine_a_cafe.js` dans un éditeur de texte comme Bloc-Notes (évitez les logiciels de bureautique qui pourraient provoquer des problèmes à l'enregistrement), et collez la suite de chiffres et de lettres à la ligne 3, entre les apostrophes à la place de COLLEZ_LE_JETON_ICI. Enregistrez et fermez le fichier.

Depuis le terminal laissé ouvert depuis tout à l'heure, tapez `npm install discord.js` puis Entrée (le bot utilise la librairie [Discord.js](https://discord.js.org/) pour fonctionner). À la fin de l'installation, tapez enfin `node machine_a_cafe.js` et laissez la fenêtre du terminal ouverte, pour que le bot se connecte à Discord.

### Redémarrer le bot.

Par défaut, il est nécessaire de redémarrer un bot à chaque redémarrage de l'ordinateur ou du serveur. Il suffit de se rendre dans le dossier spécifique depuis le terminal, de lancer la commande `node machine_a_cafe.js` et de laisser la fenêtre du terminal ouverte. Pour le couper, il faut fermer la fenêtre du terminal (ou utiliser le raccourci Ctrl-C). Pour lancer plusieurs bots, il est nécessaire de laisser plusieurs fenêtres de terminal ouvertes.

## Paramètres

La commande est personnalisable si vous souhaitez changer le prix de la boisson, ainsi que d'autres paramètres personnalisables directement dans le fichier `machine_a_cafe.js`. Les paramètres suivants sont appliqués par défaut :
* Pas de limite de salle : entrez l'identifiant d'une salle Discord pour éviter de déménager la machine à café dans toutes les pièces.
* Boissons courtes : 'cafecapsule', 'cafemoulu', 'chocolat', 'chocolait', 'chocomenthe', 'choconoisette', 'thevert'.
* Boissons longues : 'cafegrains' (la machine met plus de temps à préparer ces boissons).
* Temps de préparation d'une boisson courte : 20 secondes.
* Temps de préparation d'une boisson longue : 40 secondes.
* Heures de disponibilité de la machine : de 8 heures à 17 heures.
* Jours de non-disponibilité de la machine : Samedi et Dimanche.
* Probabilité que le café tombe par terre et qu'on vous demande de !nettoyer : 2%.
