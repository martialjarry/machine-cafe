'use strict';
// PARAMÈTRES À RENSEIGNER IMPÉRATIVEMENT.
const token = 'Njk0MjA5MTg4MTYyODMwMzc3.XoITIQ.CI_Mc6vcZtz8G4EWB6JzVDZCJKU'; // Jeton d'identification.

// Paramètres optionnels.
//const channelId = '1'; // Identifiant du salon de la machine à café (laisser vide pour transporter la machine à café dans toutes les pièces).
const channelName = 'cafet'; // Identifiant du salon de la machine à café (laisser vide pour transporter la machine à café dans toutes les pièces).
const commande = '!50centimes'; // Commande (prix) pour lancer le bot.
const boissonsCourtes = ['cafecapsule', 'cafemoulu', 'chocolat', 'chocolait', 'chocomenthe', 'choconoisette', 'thevert']; // Boissons préparées rapidement.
const boissonsLongues = ['cafegrains']; // Boissons préparées lentement.
const tempsCourt = 10000; // Temps de conception (en millisecondes) des boissons préparées rapidement.
const tempsLong = 20000; // Temps de conception (en millisecondes) des boissons préparées lentement.
const heureOuverture = 8; // Heure d'ouverture de la machine à café.
const heureFermeture = 19; // Heure de fermeture de la machine à café.
const joursFermeture = [0, 6]; // Jours de fermeture de la machine à café (Dimanche = 0, Lundi = 1, ... Samedi = 6).
const probaAccident = 2; // Pourcentages d'accidents nécessitant de !nettoyer le sol avant de se resservir.

const Discord = require('discord.js');
const client = new Discord.Client();
let encours = false;
let nettoyer = false;

client.login(token);

function cafe(msg, time, boisson) {
	encours = true;
	msg.channel.send('Vous avez sélectionné la boisson : '+boisson).catch(console.error);
	msg.channel.send('*gobelet qui tombe*').catch(console.error);
	setTimeout(() => {
		msg.channel.send('**BVUUUUU**').catch(console.error);
		setTimeout(() => {
			msg.channel.send('*BIIIIP*').catch(console.error);
			encours = false;
			var rand = (Math.floor(Math.random() * 100) + 1);
			console.log("le rand : "+rand);
			if ( rand <= probaAccident) {
				setTimeout(() => {
					nettoyer = true;
					msg.channel.send('**PLOUF** oh non, le café est tombé par terre, il faut !nettoyer.').catch(console.error);
				}, 3000);
			}
		}, time);
	}, 2000);
}

client.on('message', (msg) => {
	console.log("-- message recu --")
	console.log(msg.content)
	
	console.log("l'id du channel : '" + msg.channel.id +"'.")
	console.log("le name du channel : '" + msg.channel.name +"'.")
	
	
	if (msg.content.indexOf(commande) !== -1) {
		if (msg.channel.name === channelName) {
			var boisson = msg.content.replace(commande,'');
			const d = new Date();
			if ((d.getHours() >= heureOuverture) && (d.getHours() < heureFermeture) && (joursFermeture.indexOf(d.getDay()) === -1)) {
				if (nettoyer) {
					msg.channel.send('Il faut !nettoyer le café par terre avant de se resservir.').catch(console.error);
				} else if (encours) {
					msg.channel.send('On coupe pas la file d\'attente SVP.').catch(console.error);
				} else {
					boissonsCourtes.some((boissonCourte) => {
						if (msg.content.indexOf(boissonCourte) !== -1) {
							cafe(msg, tempsCourt, boisson);
							return true;
						}
						return false;
					});
					boissonsLongues.some((boissonlongue) => {
						if (msg.content.indexOf(boissonlongue) !== -1) {
							cafe(msg, tempsLong, boisson);
							return true;
						}
						return false;
					});
					if (!encours) {
						msg.channel.send(`Liste des boissons : ${boissonsCourtes.join(', ')}, ${boissonsLongues.join(', ')}.`).catch(console.error);
					}
				}
			}
			else if (((d.getHours() < heureOuverture) || (d.getHours() >= heureFermeture)) || (joursFermeture.indexOf(d.getDay()) !== -1)) {
				msg.channel.send('La kfet est fermée depuis '+heureFermeture+'h, prenez votre café chez vous ou revenez demain à partir de '+heureOuverture+'h.').catch(console.error);
			}
		}
		else {
				msg.channel.send('La machine à café n\'est pas dans cette salle, vos pièces sont tombées par terre.').catch(console.error);
		}
	}
	else if ((nettoyer) && (msg.content.indexOf('!nettoyer') === 0)) {
		nettoyer = false;
		msg.channel.send('Merci d\'avoir nettoyé !').catch(console.error);
	}
	else{
		console.log("pas une commande reconnue")
	}
});

process.on('error', function(err) {
	console.log(err);
});
